package plt;

import static org.junit.Assert.*;

import org.junit.Test;

public class TranslatorTest {

	@Test
	public void testInputPhrase() {
		String inputPhrase = "hello world";
		Translator translator = new Translator(inputPhrase);
		assertEquals("hello world", translator.getPhrase());
	}
	
	@Test
	public void testEmptyStringShouldBeNil() {
		String inputPhrase = "";
		Translator translator = new Translator(inputPhrase);
		assertEquals(Translator.NIL, translator.translate());
	}
	
	@Test
	public void testPhraseStartingWithVowelEndingWithY() {
		String inputPhrase = "any";
		Translator translator = new Translator(inputPhrase);
		assertEquals("anynay", translator.translate());
	}
	
	@Test
	public void testPhraseStartingWithVowelEndingWithVowel() {
		String inputPhrase = "use";
		Translator translator = new Translator(inputPhrase);
		assertEquals("useyay", translator.translate());
	}
	
	@Test
	public void testPhraseStartingWithVowelEndingWithConsonant() {
		String inputPhrase = "elicopter";
		Translator translator = new Translator(inputPhrase);
		assertEquals("elicopteray", translator.translate());
	}
	
	@Test
	public void testPhraseStartingWithSingleConsonant() {
		String inputPhrase = "coffee";
		Translator translator = new Translator(inputPhrase);
		assertEquals("offeecay", translator.translate());
	}
	
	@Test
	public void testPhraseStartingWithMoreConsonants() {
		String inputPhrase = "triggered";
		Translator translator = new Translator(inputPhrase);
		assertEquals("iggeredtray", translator.translate());
	}
	
	@Test
	public void testPhraseStartingWithMoreConsonants2() {
		String inputPhrase = "through";
		Translator translator = new Translator(inputPhrase);
		assertEquals("oughthray", translator.translate());
	}
	
	@Test
	public void testPhraseWithMoreWords() {
		String inputPhrase = "hello darkness my old friend";
		Translator translator = new Translator(inputPhrase);
		assertEquals("ellohay arknessday myay olday iendfray", translator.translateMultipleWords());
	}
	
	@Test
	public void testCompositeWords() {
		String inputPhrase = "well-being";
		Translator translator = new Translator(inputPhrase);
		assertEquals("ellway-eingbay", translator.translateMultipleWords());
	}
	
	@Test
	public void testPhraseWithMoreCompositeWords() {
		String inputPhrase = "well-being well-being";
		Translator translator = new Translator(inputPhrase);
		assertEquals("ellway-eingbay ellway-eingbay", translator.translateMultipleWords());
	}
	
	@Test
	public void testPhraseWithPunctuation() {
		String inputPhrase = "hello world!";
		Translator translator = new Translator(inputPhrase);
		assertEquals("ellohay orldway!", translator.translateMultipleWords());
	}
	
	@Test
	public void testPhraseWithPunctuation2() {
		String inputPhrase = "hi, how are you doing?";
		Translator translator = new Translator(inputPhrase);
		assertEquals("ihay, owhay areyay ouyay oingday?", translator.translateMultipleWords());
	}
}
