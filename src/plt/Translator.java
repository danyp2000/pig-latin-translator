package plt;

public class Translator {
	
	public static final String NIL = "nil";
	
	private String phrase;
	
	public Translator(String inputPhrase) {
		phrase = inputPhrase;
	}

	public String getPhrase() {
		
		return phrase;
	}

	public String translate() {
		
		if (phraseStartsWithVowel()) {
			
			if (phrase.endsWith("y")) {
				return phrase + "nay";
			}
			
			else if (phraseEndsWithVowel()) {
				return phrase + "yay";
			}
			
			else if (phraseEndsWithConsonant()) {
				return phrase + "ay";
			}
		}
			
		else if (phraseStartsWithConsonant() && phraseSecondCharNotAConsonant()) {
			char consonant = phrase.charAt(0);
			phrase = phrase.substring(1);
			return phrase + consonant + "ay";
		}
		
		else if (phraseStartsWithConsonant() && phraseSecondCharIsAConsonant()) {
			int x = 0;
			String letters = "";
			String newPhrase = phrase;
			
			do {
				letters = letters + newPhrase.charAt(0);
				newPhrase = newPhrase.substring(1);
				x++;
				
				
			} while (x != phrase.length() && phraseCharAtXIsAConsonant(x));
			
			return newPhrase + letters + "ay";
		}
			
		return NIL;
	}
	
	public Object translateMultipleWords() {
		int y = 0;
		String phraseCopy = phrase;
		String translatedPhrase = "";
		
		for (int x = y; x < phraseCopy.length(); x++) {
			
			if (sentenceCharAtXIsWhiteSpaceOrDashOrPunctuation(phraseCopy, x) && sentenceCharAtXNotWhiteSpaceOrDashOrPunctuation(phraseCopy, x-1)) {
				phrase = phraseCopy.substring(y, x);
				String translatedWord = translate();
				translatedPhrase = translatedPhrase + translatedWord + phraseCopy.charAt(x);
				y = x+1;
			}
			
			else if (sentenceCharAtXIsWhiteSpaceOrDashOrPunctuation(phraseCopy, x)) {
				translatedPhrase = translatedPhrase + phraseCopy.charAt(x);
				y = x+1;
			}
			
			else if (x == phraseCopy.length() - 1) {
				phrase = phraseCopy.substring(y, x+1);
				String translatedWord = translate();
				translatedPhrase = translatedPhrase + translatedWord;
			}
		}
		
		return translatedPhrase;
	}
	
	private boolean phraseStartsWithVowel() {
		return (phrase.startsWith("a") || phrase.startsWith("e") || phrase.startsWith("i") || phrase.startsWith("o") || phrase.startsWith("u"));
	}
	
	private boolean phraseEndsWithVowel() {
		return (phrase.endsWith("a") || phrase.endsWith("e") || phrase.endsWith("i") || phrase.endsWith("o") || phrase.endsWith("u"));
	}
	
	private boolean phraseEndsWithConsonant() {
		return (phrase.endsWith("b") || phrase.endsWith("c") || phrase.endsWith("d") || phrase.endsWith("f") || phrase.endsWith("g") || 
			    phrase.endsWith("h") || phrase.endsWith("j") || phrase.endsWith("k") || phrase.endsWith("l") || phrase.endsWith("m") ||
			    phrase.endsWith("n") || phrase.endsWith("p") || phrase.endsWith("q") || phrase.endsWith("r") || phrase.endsWith("s") ||
			    phrase.endsWith("t") || phrase.endsWith("v") || phrase.endsWith("w") || phrase.endsWith("x") || phrase.endsWith("z"));
	}
	
	private boolean phraseStartsWithConsonant() {
		return (phrase.startsWith("b") || phrase.startsWith("c") || phrase.startsWith("d") || phrase.startsWith("f") || phrase.startsWith("g") || 
				phrase.startsWith("h") || phrase.startsWith("j") || phrase.startsWith("k") || phrase.startsWith("l") || phrase.startsWith("m") ||
				phrase.startsWith("n") || phrase.startsWith("p") || phrase.startsWith("q") || phrase.startsWith("r") || phrase.startsWith("s") ||
				phrase.startsWith("t") || phrase.startsWith("v") || phrase.startsWith("w") || phrase.startsWith("x") || phrase.startsWith("y") ||
				phrase.startsWith("z"));
	}
	
	private boolean phraseSecondCharNotAConsonant() {
		return (phrase.charAt(1) != 'b' && phrase.charAt(1) != 'c' && phrase.charAt(1) != 'd' && phrase.charAt(1) != 'f' && phrase.charAt(1) != 'g' && 
				phrase.charAt(1) != 'h' && phrase.charAt(1) != 'j' && phrase.charAt(1) != 'k' && phrase.charAt(1) != 'l' && phrase.charAt(1) != 'm' &&
				phrase.charAt(1) != 'n' && phrase.charAt(1) != 'p' && phrase.charAt(1) != 'q' && phrase.charAt(1) != 'r' && phrase.charAt(1) != 's' &&
				phrase.charAt(1) != 't' && phrase.charAt(1) != 'v' && phrase.charAt(1) != 'w' && phrase.charAt(1) != 'x' && phrase.charAt(1) != 'y' &&
				phrase.charAt(1) != 'z');
	}
	
	private boolean phraseSecondCharIsAConsonant() {
		return (phrase.charAt(1) == 'b' || phrase.charAt(1) == 'c' || phrase.charAt(1) == 'd' || phrase.charAt(1) == 'f' || phrase.charAt(1) == 'g' ||
				phrase.charAt(1) == 'h' || phrase.charAt(1) == 'j' || phrase.charAt(1) == 'k' || phrase.charAt(1) == 'l' || phrase.charAt(1) == 'm' ||
				phrase.charAt(1) == 'n' || phrase.charAt(1) == 'p' || phrase.charAt(1) == 'q' || phrase.charAt(1) == 'r' || phrase.charAt(1) == 's' ||
				phrase.charAt(1) == 't' || phrase.charAt(1) == 'v' || phrase.charAt(1) == 'w' || phrase.charAt(1) == 'x' || phrase.charAt(1) == 'y' ||
				phrase.charAt(1) == 'z');
	}
	
	private boolean phraseCharAtXIsAConsonant(int x) {
		return (phrase.charAt(x) == 'b' || phrase.charAt(x) == 'c' || phrase.charAt(x) == 'd' || phrase.charAt(x) == 'f' || phrase.charAt(x) == 'g' ||
				phrase.charAt(x) == 'h' || phrase.charAt(x) == 'j' || phrase.charAt(x) == 'k' || phrase.charAt(x) == 'l' || phrase.charAt(x) == 'm' ||
				phrase.charAt(x) == 'n' || phrase.charAt(x) == 'p' || phrase.charAt(x) == 'q' || phrase.charAt(x) == 'r' || phrase.charAt(x) == 's' ||
				phrase.charAt(x) == 't' || phrase.charAt(x) == 'v' || phrase.charAt(x) == 'w' || phrase.charAt(x) == 'x' || phrase.charAt(x) == 'y' ||
				phrase.charAt(x) == 'z');
	}

	private boolean sentenceCharAtXIsWhiteSpaceOrDashOrPunctuation(String sentence, int x) {
		return (sentence.charAt(x) == '.' || sentence.charAt(x) == ',' || sentence.charAt(x) == ';' || sentence.charAt(x) == ':' ||
				sentence.charAt(x) == '?' || sentence.charAt(x) == '!' || sentence.charAt(x) == '\'' || sentence.charAt(x) == '(' || 
				sentence.charAt(x) == ')' || sentence.charAt(x) == ' ' || sentence.charAt(x) == '-');
	}
	
	private boolean sentenceCharAtXNotWhiteSpaceOrDashOrPunctuation(String sentence, int x) {
		return (sentence.charAt(x) != '.' && sentence.charAt(x) != ',' && sentence.charAt(x) != ';' && sentence.charAt(x) != ':' &&
				sentence.charAt(x) != '?' && sentence.charAt(x) != '!' && sentence.charAt(x) != '\'' && sentence.charAt(x) != '(' && 
				sentence.charAt(x) != ')' && sentence.charAt(x) != ' ' && sentence.charAt(x) != '-');
	}
}
